CREATE TABLE public.changepasswordtokens (
	uuid character varying (255) NOT NULL,
	email character varying (255) NOT NULL,
	token character varying (255) NOT NULL,
	creation_date timestamp,
	PRIMARY KEY (uuid)
);
CREATE TABLE public.conditions (
	uuid character varying (255) NOT NULL,
	metadata_uuid character varying (255) NOT NULL,
	metadata_name character varying (255) NOT NULL,
	separator character varying (255) DEFAULT NULL::character varying,
	condition character varying (255) NOT NULL,
	query_params character varying (255) NOT NULL,
	sub_query character varying (255) NOT NULL,
	metadata_multijoin_uuid character varying (255) NOT NULL,
	PRIMARY KEY (uuid)
);
CREATE TABLE public.documents (
	uuid character varying (255) NOT NULL,
	name character varying (255) NOT NULL,
	original_name character varying (255) NOT NULL,
	path character varying (255),
	mimetype character varying (255) NOT NULL,
	size numeric (12,0),
	table_name character varying (255) NOT NULL,
	table_key character varying (255) NOT NULL,
	PRIMARY KEY (uuid)
);
CREATE TABLE public.draggables (
	uuid character varying (255) NOT NULL,
	name character varying (255) NOT NULL,
	description character varying (255),
	template character varying (255),
	style character varying (255),
	image character varying (255),
	static_vars character varying (255),
	dynamic_vars character varying (255),
	PRIMARY KEY (uuid)
);
CREATE TABLE public.droppables (
	uuid character varying (255) NOT NULL,
	name character varying (255) NOT NULL,
	description character varying (255),
	html character varying (255),
	draggables character varying (255),
	static_values character varying (255),
	dynamic_values character varying (255),
	PRIMARY KEY (uuid)
);
CREATE TABLE public.extensions (
	uuid character varying (255) NOT NULL,
	name character varying (255) NOT NULL,
	icon character varying (255) NOT NULL,
	description character varying (255) NOT NULL,
	tag_name character varying (255) NOT NULL,
	library_path character varying (255),
	PRIMARY KEY (uuid)
);
CREATE TABLE public.fielddefinitions (
	uuid character varying (255) NOT NULL,
	metadata_uuid character varying (255) NOT NULL,
	metadata_name character varying (255) NOT NULL,
	name character varying (255) NOT NULL,
	label character varying (255) NOT NULL,
	type character varying (255) NOT NULL,
	input_type character varying (255) DEFAULT NULL::character varying,
	options character varying (255) DEFAULT NULL::character varying,
	group_name character varying (255) DEFAULT NULL::character varying,
	tab_name character varying (255) DEFAULT NULL::character varying,
	validations character varying (255) DEFAULT NULL::character varying,
	table_key bool DEFAULT false NOT NULL,
	input_disabled bool DEFAULT false NOT NULL,
	function_def character varying (255) DEFAULT NULL::character varying,
	sql_type character varying (255) DEFAULT NULL::character varying,
	sql_definition character varying (255) DEFAULT NULL::character varying,
	default_value character varying (255) DEFAULT NULL::character varying,
	pattern character varying (255) DEFAULT NULL::character varying,
	join_table_name character varying (100) DEFAULT NULL::character varying,
	join_table_key character varying (100) DEFAULT NULL::character varying,
	join_table_select_fields character varying (100) DEFAULT NULL::character varying,
	searchable bool DEFAULT false NOT NULL,
	search_condition character varying (100) DEFAULT NULL::character varying,
	search_field_name character varying (100) DEFAULT NULL::character varying,
	show_in_list bool DEFAULT false NOT NULL,
	PRIMARY KEY (uuid)
);
CREATE TABLE public.links (
	name character varying (255) NOT NULL,
	labels character varying (255) NOT NULL,
	metadata_name character varying (100) DEFAULT NULL::character varying,
	metadata_key character varying (255) DEFAULT NULL::character varying,
	metadata_searchable_field character varying (255) DEFAULT NULL::character varying,
	metadata_lock_field character varying (255) DEFAULT NULL::character varying,
	metadata_generated_uuid character varying (255) DEFAULT NULL::character varying,
	created bool DEFAULT false NOT NULL,
	PRIMARY KEY (name)
);
CREATE TABLE public.menu (
	uuid character varying (50) NOT NULL,
	links text,
	name character varying (200) NOT NULL,
	progetto character varying (2048) DEFAULT NULL::character varying,
	gruppo character varying (200) NOT NULL,
	PRIMARY KEY (uuid)
);
CREATE TABLE public.metadatas (
	uuid character varying (255) NOT NULL,
	table_name character varying (255) NOT NULL,
	icon character varying (100) DEFAULT NULL::character varying,
	select_fields character varying (255) DEFAULT NULL::character varying,
	search_fields character varying (255) DEFAULT NULL::character varying,
	description character varying (255) DEFAULT NULL::character varying,
	alias_table character varying (255) DEFAULT NULL::character varying,
	alias_condition character varying (255) DEFAULT NULL::character varying,
	table_key character varying (255) NOT NULL,
	table_key_type character varying (255) DEFAULT NULL::character varying,
	table_key_addition character varying (255) DEFAULT NULL::character varying,
	creation_query character varying (255) DEFAULT NULL::character varying,
	order_by character varying (255) DEFAULT NULL::character varying,
	tab_groups character varying (255) DEFAULT NULL::character varying,
	already_exist bool DEFAULT false NOT NULL,
	created bool DEFAULT false NOT NULL,
	PRIMARY KEY (uuid)
);
CREATE TABLE public.pagine (
	uuid character varying (50) NOT NULL,
	body text,
	progetto character varying (2048) DEFAULT NULL::character varying,
	name character varying (200) NOT NULL,
	PRIMARY KEY (uuid)
);
CREATE TABLE public.progetti (
	uuid character varying (50) NOT NULL,
	footer text,
	logo_piccolo character varying (2048) DEFAULT NULL::character varying,
	logo character varying (2048) DEFAULT NULL::character varying,
	name character varying (200) NOT NULL,
	header text,
	PRIMARY KEY (uuid)
);
CREATE TABLE public.roles (
	name character varying (255) NOT NULL,
	description character varying (255) NOT NULL,
	object_type character varying (255) NOT NULL,
	object_name character varying (255) DEFAULT NULL::character varying,
	action character varying (255) DEFAULT NULL::character varying,
	PRIMARY KEY (name)
);
CREATE TABLE public.selectqueries (
	uuid character varying (255) NOT NULL,
	query_name character varying (255) NOT NULL,
	with_params bool DEFAULT false,
	select_query character varying (255) DEFAULT NULL::character varying,
	PRIMARY KEY (uuid)
);
CREATE TABLE public.urlmaprules (
	uuid character varying (255) NOT NULL,
	pattern character varying (255) NOT NULL,
	access character varying (255) NOT NULL,
	http_methods character varying (255) NOT NULL,
	PRIMARY KEY (uuid)
);
CREATE TABLE public.userroles (
	username character varying (255) NOT NULL,
	role character varying (255) NOT NULL,
	PRIMARY KEY (username,role)
);
CREATE TABLE public.users (
	username character varying (255) NOT NULL,
	password character varying (255) NOT NULL,
	name character varying (255) NOT NULL,
	surname character varying (255) DEFAULT NULL::character varying,
	email character varying (255) DEFAULT NULL::character varying,
	active bool DEFAULT true,
	creation_date timestamp,
	last_update_date timestamp,
	PRIMARY KEY (username)
);
INSERT INTO public.documents (uuid,name,original_name,path,mimetype,size,table_name,table_key) VALUES ('c1567ef9-e01f-4322-8a20-50eaa95e6a0d','c1567ef9-e01f-4322-8a20-50eaa95e6a0d.jpeg','003573S01925-.jpg','{projectname}/progetti/c1567ef9-e01f-4322-8a20-50eaa95e6a0d.jpeg','image/jpeg',3000628,'progetti','asufc');
INSERT INTO public.documents (uuid,name,original_name,path,mimetype,size,table_name,table_key) VALUES ('d572871e-527f-40c8-9b75-e6ed25d0d453','d572871e-527f-40c8-9b75-e6ed25d0d453.png','logo_asufc (1).png','{projectname}/progetti/d572871e-527f-40c8-9b75-e6ed25d0d453.png','image/png',26950,'progetti','asufc');
INSERT INTO public.fielddefinitions (uuid,metadata_uuid,metadata_name,name,label,type,input_type,options,group_name,tab_name,validations,table_key,input_disabled,function_def,sql_type,sql_definition,default_value,pattern,join_table_name,join_table_key,join_table_select_fields,searchable,search_condition,search_field_name,show_in_list) VALUES ('cce581e1-d717-4b71-a864-a49dd67cb5de','d4dc658f-0c9c-44c2-ae01-9d9397d5fea5','progetti','name','name','input','text',NULL,NULL,NULL,NULL,false,false,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,true,'containss','name_containss',true);
INSERT INTO public.fielddefinitions (uuid,metadata_uuid,metadata_name,name,label,type,input_type,options,group_name,tab_name,validations,table_key,input_disabled,function_def,sql_type,sql_definition,default_value,pattern,join_table_name,join_table_key,join_table_select_fields,searchable,search_condition,search_field_name,show_in_list) VALUES ('c0a35045-5c56-4cb4-9295-fe08cc2f617a','d4dc658f-0c9c-44c2-ae01-9d9397d5fea5','progetti','footer','footer','textarea',NULL,NULL,NULL,NULL,NULL,false,false,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,false,'containss',NULL,false);
INSERT INTO public.fielddefinitions (uuid,metadata_uuid,metadata_name,name,label,type,input_type,options,group_name,tab_name,validations,table_key,input_disabled,function_def,sql_type,sql_definition,default_value,pattern,join_table_name,join_table_key,join_table_select_fields,searchable,search_condition,search_field_name,show_in_list) VALUES ('a3afa669-bcf9-4763-8eae-cccd72b3a2b9','d4dc658f-0c9c-44c2-ae01-9d9397d5fea5','progetti','header','header','textarea',NULL,NULL,NULL,NULL,NULL,false,false,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,false,'containss',NULL,false);
INSERT INTO public.fielddefinitions (uuid,metadata_uuid,metadata_name,name,label,type,input_type,options,group_name,tab_name,validations,table_key,input_disabled,function_def,sql_type,sql_definition,default_value,pattern,join_table_name,join_table_key,join_table_select_fields,searchable,search_condition,search_field_name,show_in_list) VALUES ('233a700e-e3c2-414b-9c4a-7fbbe3e220c2','d4dc658f-0c9c-44c2-ae01-9d9397d5fea5','progetti','logo','logo','media',NULL,NULL,NULL,NULL,NULL,false,false,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,false,'null',NULL,false);
INSERT INTO public.fielddefinitions (uuid,metadata_uuid,metadata_name,name,label,type,input_type,options,group_name,tab_name,validations,table_key,input_disabled,function_def,sql_type,sql_definition,default_value,pattern,join_table_name,join_table_key,join_table_select_fields,searchable,search_condition,search_field_name,show_in_list) VALUES ('32ef133b-76b6-4ef5-8dbe-62f35e67b8e4','d4dc658f-0c9c-44c2-ae01-9d9397d5fea5','progetti','logo_piccolo	','logo_piccolo	','media',NULL,NULL,NULL,NULL,NULL,false,false,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,false,'null',NULL,false);
INSERT INTO public.fielddefinitions (uuid,metadata_uuid,metadata_name,name,label,type,input_type,options,group_name,tab_name,validations,table_key,input_disabled,function_def,sql_type,sql_definition,default_value,pattern,join_table_name,join_table_key,join_table_select_fields,searchable,search_condition,search_field_name,show_in_list) VALUES ('10fa9ee7-117c-48fa-a965-8b0124d28718','76ecc002-2283-4b58-9521-6176dffbd1ce','pagine','name','name','input','text',NULL,NULL,NULL,NULL,false,false,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,true,'containss','name_containss',true);
INSERT INTO public.fielddefinitions (uuid,metadata_uuid,metadata_name,name,label,type,input_type,options,group_name,tab_name,validations,table_key,input_disabled,function_def,sql_type,sql_definition,default_value,pattern,join_table_name,join_table_key,join_table_select_fields,searchable,search_condition,search_field_name,show_in_list) VALUES ('3b00146f-768a-43ba-90ca-471bddccaf8e','76ecc002-2283-4b58-9521-6176dffbd1ce','pagine','body','body','textarea',NULL,NULL,NULL,NULL,NULL,false,false,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,false,'containss',NULL,false);
INSERT INTO public.fielddefinitions (uuid,metadata_uuid,metadata_name,name,label,type,input_type,options,group_name,tab_name,validations,table_key,input_disabled,function_def,sql_type,sql_definition,default_value,pattern,join_table_name,join_table_key,join_table_select_fields,searchable,search_condition,search_field_name,show_in_list) VALUES ('5c0e61e7-8adb-4bf4-b3f6-58751602ed05','76ecc002-2283-4b58-9521-6176dffbd1ce','pagine','progetto','progetto','join',NULL,NULL,NULL,NULL,NULL,false,false,NULL,NULL,NULL,NULL,NULL,'progetti','uuid','name',false,'',NULL,true);
INSERT INTO public.fielddefinitions (uuid,metadata_uuid,metadata_name,name,label,type,input_type,options,group_name,tab_name,validations,table_key,input_disabled,function_def,sql_type,sql_definition,default_value,pattern,join_table_name,join_table_key,join_table_select_fields,searchable,search_condition,search_field_name,show_in_list) VALUES ('35d8909e-7283-4d89-9840-2449fce7bdf4','1aeef001-5687-498e-bd77-b874d3937e18','menu','name','name','input','text',NULL,NULL,NULL,NULL,false,false,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,true,'containss','name_containss',true);
INSERT INTO public.fielddefinitions (uuid,metadata_uuid,metadata_name,name,label,type,input_type,options,group_name,tab_name,validations,table_key,input_disabled,function_def,sql_type,sql_definition,default_value,pattern,join_table_name,join_table_key,join_table_select_fields,searchable,search_condition,search_field_name,show_in_list) VALUES ('31ac5260-8719-4c5b-8e35-c9a5c69ae53a','1aeef001-5687-498e-bd77-b874d3937e18','menu','gruppo','gruppo','input','text',NULL,NULL,NULL,NULL,false,false,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,false,'containss',NULL,true);
INSERT INTO public.fielddefinitions (uuid,metadata_uuid,metadata_name,name,label,type,input_type,options,group_name,tab_name,validations,table_key,input_disabled,function_def,sql_type,sql_definition,default_value,pattern,join_table_name,join_table_key,join_table_select_fields,searchable,search_condition,search_field_name,show_in_list) VALUES ('16a5de4c-988b-4680-8c70-e875d58d9375','1aeef001-5687-498e-bd77-b874d3937e18','menu','links','links','textarea',NULL,NULL,NULL,NULL,NULL,false,false,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,false,'containss',NULL,false);
INSERT INTO public.fielddefinitions (uuid,metadata_uuid,metadata_name,name,label,type,input_type,options,group_name,tab_name,validations,table_key,input_disabled,function_def,sql_type,sql_definition,default_value,pattern,join_table_name,join_table_key,join_table_select_fields,searchable,search_condition,search_field_name,show_in_list) VALUES ('ba9d28bf-4c1a-48e3-9850-ce41466e815f','1aeef001-5687-498e-bd77-b874d3937e18','menu','progetto','progetto','join',NULL,NULL,NULL,NULL,NULL,false,false,NULL,NULL,NULL,NULL,NULL,'progetti','uuid','name',false,'',NULL,true);
INSERT INTO public.menu (uuid,links,name,progetto,gruppo) VALUES ('e1d63ef2-b128-48e9-aaff-c49be583b2f2','[
            {
               "name": "Home Page",
               "href": "/homepage",
               "type": "internal",
               "style": ""
            },
            {
               "name": "Sistema di Tracciabilità",
               "href": "http://ap01-truly.servizitalia.spa:8080/truly/",
               "type": "external",
               "style": ""
            },
            {
                "name": "Qualita",
                "href": "/gestione-qualita",
                "type": "internal",
                "style": ""
            }, {
                "name": "Gestione Richieste",
                "href": "/gestione-richieste",
                "type": "internal",
                "style": ""
            },
            {
                "name": "Business Intelligence",
                "href": "/business-intelligence",
                "type": "internal",
                "style": ""
            },
            {
                "name": "Contact Center",
                "href": "/contact-center",
                "type": "internal",
                "style": ""
            },
            {
                "name": "Repository Documentale",
                "href": "/repository-documentale",
                "type": "internal",
                "style": ""
            }

        ]','admin','{projectname}','admin');
INSERT INTO public.metadatas (uuid,table_name,icon,select_fields,search_fields,description,alias_table,alias_condition,table_key,table_key_type,table_key_addition,creation_query,order_by,tab_groups,already_exist,created) VALUES ('d4dc658f-0c9c-44c2-ae01-9d9397d5fea5','progetti','fa fa-product-hunt',NULL,NULL,'progetti',NULL,NULL,'uuid','slug','name',NULL,'name asc',NULL,false,true);
INSERT INTO public.metadatas (uuid,table_name,icon,select_fields,search_fields,description,alias_table,alias_condition,table_key,table_key_type,table_key_addition,creation_query,order_by,tab_groups,already_exist,created) VALUES ('76ecc002-2283-4b58-9521-6176dffbd1ce','pagine','fa fa-pagelines',NULL,NULL,'pagine',NULL,NULL,'uuid','slug','name',NULL,NULL,NULL,false,true);
INSERT INTO public.metadatas (uuid,table_name,icon,select_fields,search_fields,description,alias_table,alias_condition,table_key,table_key_type,table_key_addition,creation_query,order_by,tab_groups,already_exist,created) VALUES ('1aeef001-5687-498e-bd77-b874d3937e18','menu','fa fa-glass',NULL,NULL,'menu',NULL,NULL,'uuid','uuid',NULL,NULL,NULL,NULL,false,true);
INSERT INTO public.pagine (uuid,body,progetto,name) VALUES ('applicativi','
<section class="main">

<div class="main-content">
    <div class="breadcrumbs">
      <div class="container">
        <div class="col-sm-4">
          <h1>Applicativi</h1>
        </div>
      </div>
    </div>
    
    <div class="container relative">
        <div class="col-sm-12">   
<div class="row">

 <a routerLink="/business">
 <div class="col-sm-6 col-xs-12">  
          <div class="white-box clearfix applications active">
              <div class="item-wrapper">
              <div class="item">
                  <div class="icon-wrapper">
                      <img src="/assets/images/lavanolo.png" class="icon img-responsive">
                  </div>
                    <div class="content">
  <h3 class="title"><a href="http://ap01-truly.servizitalia.spa:8080/truly/" target="_blank">Lavanolo</a></h3>
<p>SVUP e SVOP</p>
                    </div>
              </div>
            </div>
          </div>
        </div>
</a>


    <a routerLink="/business">
 <div class="col-sm-6 col-xs-12">   
          <div class="white-box clearfix applications active">


            <div class="item-wrapper">
              <div class="item">
                  <div class="icon-wrapper">
                      <img src="/assets/images/strumentario_chirurgico.png" class="icon img-responsive">
                  </div>
                    <div class="content">
                  
                <h3 class="title"><a href="http://sixster-si.demo.sixcloud.it/#/bookmarks/home" target="_blank">Strumentario chirurgico</a></h3>
              </div>
              </div>
            </div>
          </div>
        </div>
</a>

 <a routerLink="/business">
  <div class="col-sm-6 col-xs-12">  
          <div class="white-box clearfix applications active">
              <div class="item-wrapper">
              <div class="item">
                  <div class="icon-wrapper">
                      <img src="/assets/images/ttr.png" class="icon img-responsive">
                  </div>
                    <div class="content">
                 <h3 class="title"><a href="http://totem.si-servizitalia.com/Trust/" target="_blank">TTR</a></h3>
              </div>
              </div>
            </div>
          </div>
        </div>

        </a>
  <a routerLink="/business">
 <div class="col-sm-6 col-xs-12"> 
          <div class="white-box clearfix applications not-active">
              <div class="item-wrapper">
              <div class="item">
                  <div class="icon-wrapper">
                      <img src="/assets/images/dpi.png" class="icon img-responsive">
                  </div>
                    <div class="content">
                <h3 class="title">DPI</h3>
              </div>
              </div>
            </div>
          </div>
        </div>
</a>

        <div class="col-sm-6 col-xs-12">   <a routerLink="/business">
          <div class="white-box clearfix applications not-active">
              <div class="item-wrapper">
              <div class="item">
                  <div class="icon-wrapper">
                      <img src="/assets/images/dispositivi_rx.png" class="icon img-responsive">
                  </div>
                    <div class="content">
                <h3 class="title">Dispositivi anti RX</h3>
              </div>
              </div>
            </div>
          </div></a>
        </div>

 <div class="col-sm-6 col-xs-12">   <a routerLink="/business">
          <div class="white-box clearfix applications not-active">
              <div class="item-wrapper">
              <div class="item">
                  <div class="icon-wrapper">
                      <img src="/assets/images/tnt.png" class="icon img-responsive">
                  </div>
                    <div class="content">
                <h3 class="title">TNT</h3>
              </div>
              </div>
            </div>
          </div></a>
        </div>
          
        <div class="col-sm-6 col-xs-12">   <a routerLink="/business">
          <div class="white-box clearfix applications not-active">
              <div class="item-wrapper">
              <div class="item">
                  <div class="icon-wrapper">
                      <img src="/assets/images/armadio_intervento.png" class="icon img-responsive">
                  </div>
                  <div class="content">

                <h3 class="title">Armadio intervento</h3>
                  </div>
              </div>
            </div>
          </div></a>
        </div>

  <div class="col-sm-6 col-xs-12">   <a routerLink="/business">
          <div class="white-box clearfix applications not-active">
              <div class="item-wrapper">
              <div class="item">
                  <div class="icon-wrapper">
                      <img src="/assets/images/endoscopia.png" class="icon img-responsive">
                  </div>
                    <div class="content">
                <h3 class="title">Endoscopia</h3>
                    </div>
              </div>
            </div>
          </div></a>
        </div>

  <div class="col-sm-6 col-xs-12">   <a routerLink="/business">
          <div class="white-box clearfix applications not-active">
              <div class="item-wrapper">
              <div class="item">
                  <div class="icon-wrapper">
                      <img src="/assets/images/ignegneria_clinica.png" class="icon img-responsive">
                  </div>
                    <div class="content">
                <h3 class="title">Ingegneria clinica</h3>
                    </div>
              </div>
            </div>
          </div></a>
        </div>
 <div class="col-sm-6 col-xs-12">   <a routerLink="/business">
          <div class="white-box clearfix applications not-active">
              <div class="item-wrapper">
              <div class="item">
                  <div class="icon-wrapper">
                      <img src="/assets/images/gestione_capi_ospiti.png" class="icon img-responsive">
                  </div>
                    <div class="content">
                <h3 class="title">Gestione Capi Ospite e di Proprieta''</h3>
              </div>
              </div>
            </div>
          </div></a>
        </div>
            
     
            <div class="col-sm-6 col-xs-12">   <a routerLink="/business">
          <div class="white-box clearfix applications not-active">
              <div class="item-wrapper">
              <div class="item">
                  <div class="icon-wrapper">
                      <img src="/assets/images/depositivi_antidecubito.png" class="icon img-responsive">
                  </div>
                    <div class="content">
                <h3 class="title">Dispositivi antidecubito</h3>
                    </div>
              </div>
            </div>
          </div></a>
        </div>
       

  <div class="col-sm-6 col-xs-12">   <a routerLink="/business">
          <div class="white-box clearfix applications not-active">
              <div class="item-wrapper">
              <div class="item">
                  <div class="icon-wrapper">
                      <img src="/assets/images/logistica.png" class="icon img-responsive">
                  </div>
                    <div class="content">
                <h3 class="title">Altro (Logistica, Gestione Spogliatoi, etc)</h3>
                    </div>
              </div>
            </div>
          </div></a>
        </div>



      </div>
    </div>
  </div>
</section>
','{projectname}','applicativi');
INSERT INTO public.pagine (uuid,body,progetto,name) VALUES ('business-intelligence','
<section class="main">
<div class="main-content">
    <div class="breadcrumbs">
      <div class="container">
        <div class="col-sm-4">
          <h1>Business Intelligence</h1>
        </div>
      </div>
    </div>
    
    <div class="container relative">
        <div class="col-sm-12">   
          <div class="white-box-content clearfix">
        <div class="col-sm-12">   
<br>
<img class="icon img-responsive" src="https://veneto.si-portal.it/snello-api/api/documents/66a07a67-6d14-4016-9648-841107bf9171/download">

            </div>
   </div>
  </div>
</section>
','{projectname}','business intelligence');
INSERT INTO public.pagine (uuid,body,progetto,name) VALUES ('contact-center','
<section class="main">
<div class="main-content">
    <div class="breadcrumbs">
      <div class="container">
        <div class="col-sm-4">
          <h1>Contact Center</h1>
        </div>
      </div>
    </div>
    
    <div class="container relative">
        <div class="col-sm-12">   
      <div class="list-wrapper">
        <div class="row">

      <div class="white-box-content clearfix">
<img class="icon img-responsive" src="/assets/images/call.jpg">
<br/>
<br/>
              S.P.O.C. (Single Point Of Contact) - operatore unico a disposizione del cliente attraverso:<br/>
Numero telefonico dedicato fruibile negli orari di disponibilità del servizio<br/>
Indirizzo di posta elettronica dedicato fruibile 24 ore su 24<br/>
<br/>
<br/>
            </div>
      <div class="clearfix"></div>
      </div>
    </div>
  </div>
</section>
','{projectname}','contact center');
INSERT INTO public.pagine (uuid,body,progetto,name) VALUES ('cookies','
<section class="main">
<div class="main-content">
    <div class="breadcrumbs">
      <div class="container">
        <div class="col-sm-4">
          <h1>Cookies</h1>
        </div>
      </div>
    </div>
    
    <div class="container relative">
        <div class="col-sm-12">   
      <div class="list-wrapper">
        <div class="row">

      <div class="white-box-content text-default clearfix">
<p><span style="text-decoration: underline;">COOKIE POLICY</span></p>
<p>Il sito web <a href="http://www.si-serviziitalia.it">www.si-servizitalia.com</a> (in seguito, &ldquo;Sito&rdquo;) fa uso di cookie. Di seguito potr&agrave; trovare maggiori informazioni in merito ai cookie, a come vengono utilizzati nel Sito e a quali procedure di controllo adottiamo rispetto ai cookie stessi. Proseguendo nella navigazione nel Sito, dopo aver letto il nostro banner, acconsente all&rsquo;utilizzo dei cookie, in conformit&agrave; con la presente informativa. Se non &egrave; d&rsquo;accordo all&rsquo;utilizzo dei cookie, La invitiamo a disabilitarli seguendo le istruzioni di seguito riportate nella presente informativa.</p>
<ol>
<li><span style="text-decoration: underline;">CHE COSA SONO I COOKIE?</span></li>
</ol>
<p>I cookie sono piccoli <em>files</em> di testo che vengono salvati sul Suo dispositivo elettronico (PC, tablet, etc.) durante la navigazione in un sito web e che forniscono determinate informazioni (&ldquo;Cookies&rdquo;). I Cookies vengono poi rinviati al sito web di origine nel corso di ogni successiva navigazione, oppure vengono inviati ad un diverso sito web che sia in grado di riconoscere quello specifico Cookie. I Cookies agiscono come una memoria per un sito web, consentendo che il sito web riconosca il Suo <em>device</em> ad ogni Sua visita successiva a quel medesimo sito. I Cookies consentono inoltre di memorizzare le Sue preferenze di navigazione offrendoLe un&rsquo;esperienza pi&ugrave; funzionale del Sito, e rendendo il contenuto del sito il pi&ugrave; personalizzato possibile.</p>
<p>I browser web Le consentono di esercitare un certo controllo sui Cookies attraverso le impostazioni del browser. La maggior parte dei browser consente di bloccare i Cookies o bloccare i Cookies di determinati siti. I browser possono anche aiutarLa a eliminare i Cookies quando chiude il browser. Tuttavia, dovrebbe tenere a mente che ci&ograve; potrebbe significare che eventuali opt-out o preferenze che ha impostato sul sito andranno persi. La invitiamo a consultare le informazioni tecniche relative al Suo browser per le istruzioni. Se sceglie di disabilitare l''impostazione dei Cookies o se rifiuta di accettare un Cookie, alcune parti del servizio potrebbero non funzionare correttamente o potrebbero essere notevolmente pi&ugrave; lente.</p>
<ol>
<li><span style="text-decoration: underline;">TIPOLOGIE DI COOKIE</span></li>
</ol>
<p>&nbsp;</p>
<p>I Cookies possono essere tecnici, analitici e di profilazione.</p>
<p>Il Sito utilizza <strong>cookies tecnici &ldquo;di sessione&rdquo; e persistenti proprietari</strong>, quali:</p>
<ul>
<li>Cookie di navigazione: Sono cookie che garantiscono la normale navigazione e fruizione del sito web e che permettono il collegamento tra il server e il browser dell''utente. Questi cookie permettono al sito di funzionare correttamente e consentono di visualizzare i contenuti sul dispositivo utilizzato. Senza questi cookies alcune funzionalit&agrave; richieste come ad esempio il log-in al sito o la creazione di un carrello per lo shopping on line potrebbero non essere fornite.&nbsp;I cookie di navigazione sono cookie tecnici e sono necessari al funzionamento del sito.</li>
<li>Cookie funzionali: sono cookie memorizzati nel computer o altro dispositivo che, in base alla richiesta dell''utente, registrano le scelte dello stesso, per permettergli di ricordargliele al fine di ottimizzare e fornire una navigazione migliorativa e personalizzata all''interno del servizio o dell''accesso al presente sito (es registrazione della password in aree riservate, registrazione i prodotti nel carrello per avere la possibilit&agrave; di ritrovarseli nella successiva sessione, salvare la lingua selezionata, visualizzazione di un video o la possibilit&agrave; di commentare il blog ecc.).&nbsp;<em>I cookie funzionali non sono indispensabili al funzionamento del sito, ma migliorano la qualit&agrave; e l''esperienza della navigazione.</em></li>
</ul>
<p>Il Sito utilizza altres&igrave; <strong>cookies analitici di terza parte</strong> quali:</p>
<ul>
<li>Google Analytics, un servizio di analisi statistica erogato e gestito da Google Inc. (in seguito &ldquo;Google&rdquo;. Google Analytics usa dei cookie che aiutano nell''analisi di utilizzo del Sito da parte di altri utenti. Le informazioni generate dai cookies saranno cedute a Google che le custodir&agrave; nei propri server negli USA. Google elaborer&agrave; queste informazioni per nostro conto, al fine di valutare l&rsquo;utilizzo che gli utenti fanno del nostro Sito, creando dei report aggregati e anonimi relativi alle attivit&agrave; effettuate sul Sito e fornendo altri servizi connessi a queste attivit&agrave;. Eventualmente Google trasmetter&agrave; queste informazioni a terzi, a condizione che questo sia prescritto per legge o nella misura in cui questi soggetti terzi utilizzino questi dati per conto di Google. Tuttavia, in nessun caso Google collegher&agrave; il tuo indirizzo IP con altri dati di Google. &Egrave; possibile impedire l&rsquo;utilizzo dei cookies, modificando le impostazioni del browser, tenendo presente che in tal caso, potreste per&ograve; non essere pi&ugrave; in grado di utilizzare tutte le funzionalit&agrave; del Sito. Attraverso l''utilizzo del Sito esprimi il tuo consenso all''elaborazione dei dati da parte di Google nel modo sopra descritto e per il suddetto scopo. Per maggiori informazioni <a href="http://www.google.com/analytics/terms/it.html">http://www.google.com/analytics/terms/it.html</a>.</li>
<li>Google Maps, un servizio erogato e gestito da Google Inc. (in seguito &ldquo;Google&rdquo;) per fornire informazioni dettagliate sulla localizzazione della sede di Servizi Italia S.p.A. (per maggiori informazioni visitare la privacy policy combinata di Google, che comprende anche informazioni sui cookie di Google Maps <a href="http://www.google.co.uk/intl/it_IT/policies/">http://www.google.co.uk/intl/it_IT/policies/</a>).</li>
<li>Flash Cookie, in alcune circostanze il presente sito pu&ograve; usare Adobe Flash Player per erogare alcuni contenuti, come video clip o animazioni. I Flash cookie sono archiviati sul dispositivo dell''utente, ma sono gestiti attraverso un''interfaccia differente rispetto a quella fornita dal browser utilizzato.</li>
</ul>
<p>Il Sito <strong>non</strong> utilizza cookies di profilazione.</p>
<ol>
<li><span style="text-decoration: underline;">FINALIT&Agrave;</span></li>
</ol>
<p>&nbsp;</p>
<p>I Cookies tecnici usati dal nostro Sito servono a effettuare e facilitare la tua navigazione e a fornirLe e a permetterLe di fruire dei servizi del Sito.</p>
<p>&nbsp;</p>
<p>I Cookies analitici servono ad analizzare e monitorare il modo in cui utilizza il Sito (ad es. n. accessi e pagine viste), a fini statistici e per permetterci di apportare modifiche migliorative al Sito in termini di funzionamento e navigazione.</p>
<p>&nbsp;</p>
<p>Il conferimento dei dati &egrave; facoltativo. Si fa tuttavia presente che il rifiuto/disattivazione dei cookies potr&agrave; impedire di utilizzare alcune funzionalit&agrave; del Sito.</p>
<p>&nbsp;</p>
<ol>
<li><span style="text-decoration: underline;">COME CONTROLLARE ED ELIMINARE I COOKIE </span></li>
</ol>
<p>&nbsp;</p>
<p>Pu&ograve; autorizzare l&rsquo;utilizzo dei Cookies proseguendo nella navigazione sul Sito, dopo aver letto il Banner. Pu&ograve; non autorizzare l&rsquo;utilizzo di Cookie o comunque disattivarli in ogni momento procedendo manualmente alla modificazione della configurazione del Suo browser e seguendo le istruzioni contenute nelle policy dei gestori dei Cookie. &nbsp;</p>
<p>&nbsp;</p>
<p>Come disabilitare i cookie</p>
<p>Avvertenza: il conferimento dei dati &egrave; facoltativo; si fa tuttavia presente che il rifiuto/disattivazione dei cookies potr&agrave; impedire di utilizzare alcune funzionalit&agrave; del Sito.</p>
<ul>
<li>Google<br /> Per disabilitare l&rsquo;utilizzo dei cookie analitici ed impedire a Google Analitycs di raccogliere i dati di navigazione &egrave; possibile scaricare il componente disponibile al seguente link: <a href="https://tools.google.com/dlpage/gaoptout" target="_blank">https://tools.google.com/dlpage/gaoptout</a><br /> Per disabilitare l&rsquo;utilizzo dei cookie da parte di Google visitare la pagina: <a href="http://www.google.com/settings/ads" target="_blank">http://www.google.com/settings/ads</a></li>
<li>Per disabilitare direttamente dal browser le tipologie di cookie utilizzate dal presente sito web<br /> Chrome: <a href="https://support.google.com/chrome/answer/95647?hl=it" target="_blank">https://support.google.com/chrome/answer/95647?hl=it</a><br /> Internet Explorer: <a href="http://windows.microsoft.com/it-it/windows7/how-to-manage-cookies-in-internet-explorer-9" target="_blank">http://windows.microsoft.com/it-it/windows7/how-to-manage-cookies-in-internet-explorer-9</a><br /> Opera: <a href="http://help.opera.com/Windows/10.00/it/cookies.html" target="_blank">http://help.opera.com/Windows/10.00/it/cookies.html</a><br /> Safari: <a href="http://support.apple.com/kb/HT1677?viewlocale=it_IT" target="_blank">http://support.apple.com/kb/HT1677?viewlocale=it_IT</a><br /> Firefox: <a href="https://support.mozilla.org/it/kb/Attivare%20e%20disattivare%20i%20cookie" target="_blank">https://support.mozilla.org/it/kb/Attivare e disattivare i cookie</a></li>
</ul>
<p>Se si utilizza un browser Web non presente nell''elenco riportato sopra, si prega di fare riferimento alla documentazione o alla guida online del proprio browser per ottenere ulteriori informazioni. Si avvisano gli utenti che il Titolare funge da mero intermediario tecnico per i link riportati in questo documento e non pu&ograve; assumersi nessuna responsabilit&agrave; in caso di eventuale modifica.</p>
<p>&nbsp;</p>
<p>Potr&agrave; trovare maggiori informazioni sui Cookies, incluso come &egrave; possibile capire che cosa i Cookies abbiano impostato sul Suo <em>device</em>, e come &egrave; possibile gestirli ed eliminarli, accedendo al seguente link <a href="http://www.aboutcookies.org"><em>www.aboutcookies.org</em></a></p>
<p>Per informazioni su: Modalit&agrave; del trattamento, Accesso ai dati, Comunicazione dei dati, Diritti dell&rsquo;interessato, Modalit&agrave; di esercizio dei diritti, Titolare, responsabili e incaricati, <span style="text-decoration: underline;">legga qui la nostra Informativa Privacy.</span></p>
<p>Contatti: Per qualunque informazione in merito alla presente informativa, ci contatti al seguente indirizzo e-mail privacy@si-servizitalia.com; ci mandi una raccomandata a.r. a Servizi Italia S.p.A., in Castellina di Soragna (PR), Via San Pietro 59/B &ndash; 43019 o ci chiami al numero (+39) 0524 598511.</p>
            </div>
      <div class="clearfix"></div>
      </div>
    </div>
  </div>
</section>
','{projectname}','cookies');
INSERT INTO public.pagine (uuid,body,progetto,name) VALUES ('gestione-qualita','
<section class="main">
<div class="main-content">
    <div class="breadcrumbs">
      <div class="container">
        <div class="col-sm-4">
          <h1>Gestione qualità</h1>
        </div>
      </div>
    </div>
    
    <div class="container relative">
        <div class="col-sm-12">   
          <div class="white-box-content clearfix">
        <div class="col-sm-12">   <br><br>
<p style="text-align: left">In questa sezione è consultabile la gestione delle Non Conformità: </p>
<ul class="list-group"> 
<li class="list-group-item text-left"><a href="/#/gestione-richieste"><img style="height: 50px;" src="/assets/images/richieste.svg"/> Comunicazione e Monitoraggio <b>(accedi)</b></a> </li>
<li class="list-group-item text-left"><a href="/#/repository-documentale"><img style="height: 50px;" src="/assets/images/documents.svg"/>la documentazione relativa alle Certificazioni di qualità, ambiente e sicurezza  <b>(accedi)</b></a></li>.
</ul>
<br/>
<br/>
            </div>
   </div>
  </div>
</section>
','{projectname}','gestione qualita');
INSERT INTO public.pagine (uuid,body,progetto,name) VALUES ('homepage','
<section class="main">

<div class="main-content">
    <div class="breadcrumbs">
      <div class="container">
        <div class="col-sm-4">
          <h1>Benvenuto</h1>
        </div>
      </div>
    </div>
    
    <div class="container relative">
        <div class="col-sm-12">       

<div class="banner-box blue clearfix">
              
          <div class="col-sm-4 no-margin">
              <div class="logo-box">
                <img src="/snello-api/api/documents/d572871e-527f-40c8-9b75-e6ed25d0d453/download" class="icon img-responsive">
             </div>
          </div>

          <div class="col-sm-8 no-margin">    
<div class="image-home">
<img src="/snello-api/api/documents/c1567ef9-e01f-4322-8a20-50eaa95e6a0d/download" class="img-responsive object-fit_cover"">           
          </div>
</div>
        </div>


  <div class="other-applications">
      <div class="col-sm-12 col-xs-12 no-margin">
        <div class="sub-title"><br>
        <h3 class="text-left">Le nostre soluzioni</h3>
      </div>
      </div>

      <div class="row equal">
          <div class="col-sm-6 col-xs-12">   <a href="https://truly.si-servizitalia.com/totem/" target="_blank">
            <div class="white-box clearfix applications">
              <div class="item-wrapper">
                <div class="item">
                  <div class="icon-wrapper">
                      <img src="/assets/images/app.svg" class="icon img-responsive">
                  </div>
                    <div class="content"><h3 class="title">Tracciabilità</h3>
                       <p style="font-size:0.4em; font-style:italyc;">Gestione e tracciabilità dell''intero processo</p>
                </div>
              </div>
              </div>
            </div></a>
          </div>

          <div class="col-sm-6 col-xs-12">   <a href="/#/gestione-qualita">
            <div class="white-box clearfix applications">
              <div class="item-wrapper">
                <div class="item">
                    <div class="icon-wrapper">
                        <img src="/assets/images/quality.svg" class="icon img-responsive">
                      </div>
                        <div class="content">
                    <h3 class="title">Qualità</h3>
                       <p style="font-size:0.4em; font-style:italyc;">Gestione dei controlli qualità</p>
                </div>
                </div>
              </div>
            </div></a>
          </div>

	 <div class="col-sm-6 col-xs-12">    <a href="/#/gestione-richieste">
            <div class="white-box clearfix applications">
              <div class="item-wrapper clearfix">
                <div class="item">
                    <div class="icon-wrapper">
                        <img src="/assets/images/richieste.svg" class="icon img-responsive">
                      </div>
                        <div class="content">
                  <h3 class="title">Gestione Richieste</h3>
                       <p style="font-size:0.4em; font-style:italyc;">Gestione delle richieste/segnalazioni</p>
                </div>
                </div>
              </div>
            </div> </a>
          </div>


          <div class="col-sm-6 col-xs-12">   <a href="/#/business-intelligence">
            <div class="white-box clearfix applications">
              <div class="item-wrapper">
                <div class="item">
                    <div class="icon-wrapper">
                        <img src="/assets/images/business_intelligence.svg" class="icon img-responsive">
                      </div>
                        <div class="content">
                    <h3 class="title">Business Intelligence</h3>
                       <p style="font-size:0.4em; font-style:italyc;">Statistiche sui prodotti/servizi</p>
                </div>
                </div>
              </div>
            </div></a>
          </div>

	 <div class="col-sm-6 col-xs-12">    <a href="/#/contact-center">
            <div class="white-box clearfix applications">
              <div class="item-wrapper">
                <div class="item">
                    <div class="icon-wrapper">
                        <img src="/assets/images/contact_center.svg" class="icon img-responsive">
                      </div>
                        <div class="content">
                    		<h3 class="title">Contact Center</h3>
                	</div>
                </div>
              </div>
            </div></a>
          </div>


          <div class="col-sm-6 col-xs-12">   <a href="/#/repository-documentale">
            <div class="white-box clearfix applications">
              <div class="item-wrapper">
                <div class="item">
                    <div class="icon-wrapper">
                        <img src="/assets/images/documents.svg" class="icon img-responsive">
                      </div>
                        <div class="content">
                    <h3 class="title">Repository Documentale</h3>
                       <p style="font-size:0.4em; font-style:italyc;">Documentazione</p>
                </div>
              </div>
              </div>
            </div></a>
          </div>
         
                 </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</section>','{projectname}','homepage');
INSERT INTO public.pagine (uuid,body,progetto,name) VALUES ('mappa-del-sito','
<section class="main">
<div class="main-content">
    <div class="breadcrumbs">
      <div class="container">
        <div class="col-sm-4">
          <h1>Mappa del Sito</h1>
        </div>
      </div>
    </div>
    
    <div class="container relative">
        <div class="col-sm-12">   
      <div class="list-wrapper">
        <div class="row">

      <div class="white-box-content text-default clearfix">
<ul class="list-unstyled components"><!---->
<li><a href="/#//homepage" target="" style="">Home Page</a></li><br>
<li><a href="http://ap01-truly.servizitalia.spa:8080/truly/" target="" style="">Sistema di tracciabilità</a></li><br>
<li><a href="/#//gestione-qualita" target="" style="">Gestione qualita</a></li><br>
<li><a href="/#//gestione-richieste" target="" style="">Gestione Richieste</a></li><br>
<li><a href="/#//repository-documentale" target="" style="">Repository Documentale</a></li><br>
<li><a href="/#//contact-center" target="" style="">Contact Center</a></li><br>

</ul>
<br/>
<br/>
            </div>
      <div class="clearfix"></div>
      </div>
    </div>
  </div>
</section>
','{projectname}','mappa del sito');
INSERT INTO public.pagine (uuid,body,progetto,name) VALUES ('note-legali','
<section class="main">
<div class="main-content">
    <div class="breadcrumbs">
      <div class="container">
        <div class="col-sm-4">
          <h1>Note legali</h1>
        </div>
      </div>
    </div>
    
    <div class="container relative">
        <div class="col-sm-12">   
      <div class="list-wrapper">
        <div class="row">

      <div class="white-box-content text-default clearfix">
note legali
<br/>
<br/>
            </div>
      <div class="clearfix"></div>
      </div>
    </div>
  </div>
</section>
','{projectname}','note legali');
INSERT INTO public.pagine (uuid,body,progetto,name) VALUES ('privacy','
<section class="main">
<div class="main-content">
    <div class="breadcrumbs">
      <div class="container">
        <div class="col-sm-4">
          <h1>Privacy</h1>
        </div>
      </div>
    </div>
    
    <div class="container relative">
        <div class="col-sm-12">   
      <div class="list-wrapper">
        <div class="row">

      <div class="white-box-content text-default clearfix">
<p align="left"><strong><span style="text-decoration: underline;">Informativa per il trattamento dei dati personali dei clienti </span></strong></p>
<p align="left"><strong><span style="text-decoration: underline;">&nbsp;</span></strong></p>
<p align="left"><strong>Servizi Italia S.p.A.</strong>, con sede legale in Castellina di Soragna (PR), Via San Pietro 59/B &ndash; 43019, C.F. 08531760158 e P.IVA 02144660343, in qualit&agrave; di titolare del trattamento (in seguito, &ldquo;<strong>Titolare</strong>&rdquo;), La informa ai sensi del Regolamento UE 2016/679 (&ldquo;<strong>GDPR</strong>&rdquo;) e della vigente normativa nazionale in materia di protezione dei dati personali che i Suoi dati saranno trattati con le modalit&agrave; e per le finalit&agrave; seguenti:</p>
<p align="left">&nbsp;</p>
<ol start="1">
<li><strong><span style="text-decoration: underline;">Oggetto del trattamento</span></strong></li>
</ol>
<p align="left"><strong><span style="text-decoration: underline;">&nbsp;</span></strong></p>
<p align="left">Il Titolare tratta i dati personali, identificativi e non sensibili (in particolare, nome, cognome, indirizzo, email, numero di telefono, etc.) da Lei comunicati in occasione della conclusione del rapporto contrattuale con il Titolare o anche successivamente, nonch&eacute; i dati identificativi e non sensibili (in particolare, nome, cognome, taglia, etc.) dei Suoi dipendenti e/o collaboratori ove necessari per l&rsquo;esecuzione del contratto (in seguito, &ldquo;<strong>Dati Personali</strong>&rdquo; o anche &ldquo;<strong>Dati</strong>&rdquo;).</p>
<p align="left">&nbsp;</p>
<ol start="2">
<li><strong><span style="text-decoration: underline;">Finalit&agrave; e basi giuridiche del trattamento</span></strong></li>
</ol>
<p align="left"><strong><span style="text-decoration: underline;">&nbsp;</span></strong></p>
<p align="left">I Suoi Dati Personali sono trattati, senza Suo previo consenso, per le seguenti finalit&agrave; e basi giuridiche:</p>
<p align="left">&nbsp;</p>
<ul>
<li>l&rsquo;esecuzione del contratto e/o l&rsquo;adempimento di impegni precontrattuali, in particolare per:</li>
</ul>
<p align="left">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; la gestione dei rapporti precontrattuali e contrattuali;</p>
<p align="left">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; l&rsquo;esecuzione del contratto;</p>
<p align="left">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; la gestione degli incassi e dei pagamenti.</p>
<p align="left">&nbsp;</p>
<ul>
<li>l&rsquo;adempimento da parte del Titolare di obblighi di legge, quali:</li>
</ul>
<p align="left">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; l&rsquo;ottemperanza agli obblighi previsti da leggi, regolamenti o dalla normativa nazionale e comunitaria ovvero imposti dalle Autorit&agrave; competenti;</p>
<p align="left">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; la compilazione ed elaborazione delle dichiarazioni fiscali e degli adempimenti alle stesse connesse;</p>
<p align="left">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; la tenuta della contabilit&agrave; e degli adempimenti ad essa relativi.</p>
<p align="left">&nbsp;</p>
<ul>
<li>il perseguimento di un legittimo interesse del Titolare, in particolare:</li>
</ul>
<p align="left">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; l&rsquo;esercizio dei diritti del Titolare in sede giudiziaria e la gestione degli eventuali contenziosi;</p>
<p align="left">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; la prevenzione e repressione di atti illeciti.</p>
<p align="left">&nbsp;</p>
<ol start="3">
<li><strong><span style="text-decoration: underline;">Modalit&agrave; del trattamento</span></strong></li>
</ol>
<p align="left"><strong><span style="text-decoration: underline;">&nbsp;</span></strong></p>
<p align="left">Il trattamento dei Suoi Dati Personali &egrave; realizzato, con modalit&agrave; elettroniche e cartacee, per mezzo delle operazioni di raccolta, registrazione, organizzazione, conservazione, consultazione, elaborazione, modificazione, selezione, estrazione, raffronto, utilizzo, interconnessione, blocco, comunicazione, cancellazione e distruzione dei dati.</p>
<p align="left">&nbsp;</p>
<ol start="4">
<li><strong><span style="text-decoration: underline;">Conservazione dei Dati </span></strong></li>
</ol>
<p align="left">&nbsp;</p>
<p align="left">Il Titolare tratter&agrave; i Dati Personali per il tempo necessario per adempiere alle finalit&agrave; di cui sopra e comunque per non oltre 10 anni dalla cessazione del rapporto contrattuale.</p>
<p align="left">&nbsp;</p>
<ol start="5">
<li><strong><span style="text-decoration: underline;">Conferimento dei Dati</span></strong></li>
</ol>
<p align="left"><strong><span style="text-decoration: underline;">&nbsp;</span></strong></p>
<p align="left">Il conferimento dei Dati &egrave; obbligatorio e l&rsquo;eventuale rifiuto di fornire tali Dati comporta l&rsquo;impossibilit&agrave; di instaurare o proseguire il rapporto contrattuale col Titolare.&nbsp;&nbsp;&nbsp;&nbsp;</p>
<p align="left"><strong><span style="text-decoration: underline;">&nbsp;</span></strong></p>
<ol start="6">
<li><strong><span style="text-decoration: underline;">Accesso ai Dati</span></strong></li>
</ol>
<p align="left"><strong>&nbsp;</strong></p>
<p align="left">I Suoi Dati potranno essere resi accessibili per le finalit&agrave; di cui sopra a:</p>
<ul>
<li>dipendenti e/o collaboratori del Titolare, nella loro qualit&agrave; di incaricati del trattamento e/o responsabili interni del trattamento e/o amministratori di sistema;</li>
<li>societ&agrave; del Gruppo e soggetti terzi (ad esempio, istituti di credito, studi professionali, etc.) che svolgono attivit&agrave; in <em>outsourcing</em> per conto del Titolare, nella loro qualit&agrave; di responsabili esterni del trattamento.</li>
</ul>
<p align="left">&nbsp;</p>
<ol start="7">
<li><strong><span style="text-decoration: underline;">Comunicazione dei Dati </span></strong></li>
</ol>
<p align="left"><strong><span style="text-decoration: underline;">&nbsp;</span></strong></p>
<p align="left">I Suoi Dati possono essere comunicati, anche senza Suo consenso, a organi di controllo, forze dell&rsquo;ordine o magistratura Ministero delle finanze, Agenzia delle Entrate, enti ministeriali e Autorit&agrave; competenti, Enti locali (regioni, province, comuni), Commissioni tributarie regionali e provinciali, su loro espressa richiesta che li tratteranno in qualit&agrave; di autonomi titolari del trattamento per finalit&agrave; istituzionali e/o in forza di legge nel corso di indagini e controlli. I Suoi Dati possono essere altres&igrave; comunicati a soggetti terzi (ad esempio, partner, liberi professionisti, etc.), in qualit&agrave; di autonomi titolari del trattamento, per lo svolgimento di attivit&agrave; strumentali alle finalit&agrave; di cui sopra.</p>
<p align="left">&nbsp;</p>
<ol start="8">
<li><strong><span style="text-decoration: underline;">Trasferimento dei Dati</span></strong></li>
</ol>
<p align="left">&nbsp;</p>
<p align="left">I Dati non sono diffusi n&eacute; trasferiti in paesi extra UE.</p>
<p align="left">&nbsp;</p>
<ol start="9">
<li><strong><span style="text-decoration: underline;">Diritti dell&rsquo;interessato </span></strong></li>
</ol>
<p align="left">&nbsp;</p>
<p align="left">Il Titolare La informa che, in qualit&agrave; di soggetto interessato, se non ricorrono le limitazioni previste dalla legge, ha il diritto di:</p>
<ul>
<li>ottenere la conferma dell&rsquo;esistenza o meno di Suoi dati personali, anche se non ancora registrati, e che tali dati vengano messi a Sua disposizione in forma intellegibile;</li>
<li>ottenere indicazione e, se del caso, copia: a) dell&rsquo;origine e della categoria dei dati personali; b) della logica applicata in caso di trattamento effettuato con l''ausilio di strumenti elettronici; c) delle finalit&agrave; e modalit&agrave; del trattamento; d) degli estremi identificativi del titolare e dei responsabili; e) dei soggetti o delle categorie di soggetti ai quali i dati personali possono essere comunicati o che possono venirne a conoscenza, in particolare se destinatari di paesi terzi o organizzazioni internazionali; e) quando possibile, del periodo di conservazione dei dati oppure i criteri utilizzati per determinare tale periodo; f) dell&rsquo;esistenza di un processo decisionale automatizzato, compresa la profilazione, e in tal caso delle logiche utilizzate, dell&rsquo;importanza e delle conseguenze previste per l&rsquo;interessato; g) dell&rsquo;esistenza di garanzie adeguate in caso di trasferimento dei dati a un paese extra-UE o a un&rsquo;organizzazione internazionale;</li>
<li>ottenere, senza ingiustificato ritardo, l&rsquo;aggiornamento e la rettifica dei dati inesatti ovvero, quando vi ha interesse, l&rsquo;integrazione dei dati incompleti;</li>
<li>revocare in ogni momento i consensi prestati, con facilit&agrave;, senza impedimenti, utilizzando, se possibile, gli stessi canali usati per fornirli;</li>
<li>ottenere la cancellazione, la trasformazione in forma anonima o il blocco dei dati: a) trattati illecitamente; b) non pi&ugrave; necessari in relazione agli scopi per i quali sono stati raccolti o successivamente trattati; c) in caso di revoca del consenso su cui si basa il trattamento e in caso non sussista altro fondamento giuridico, d) qualora Lei si sia opposto al trattamento e non sussiste alcun motivo legittimo prevalente per proseguire il trattamento; e) in caso di adempimento di un obbligo legale; f) nel caso di dati riferiti a minori. Il Titolare pu&ograve; rifiutare la cancellazione solo nel caso di: a) esercizio del diritto alla libert&agrave; di espressione e di informazione; b) adempimento di un obbligo legale, esecuzione di un compito svolto nel pubblico interesse o esercizio di pubblici poteri; c) motivi di interesse sanitario pubblico; d) archiviazione nel pubblico interesse, ricerca scientifica o storica o a fini statistici; e) esercizio di un diritto in sede giudiziaria;</li>
<li>ottenere la limitazione del trattamento nel caso di: a) contestazione dell&rsquo;esattezza dei dati personali; b) trattamento illecito del Titolare per impedirne la cancellazione; c) esercizio di un Suo diritto in sede giudiziaria; d) verifica dell&rsquo;eventuale prevalenza dei motivi legittimi del Titolare rispetto a quelli dell&rsquo;interessato;</li>
<li>ricevere, qualora il trattamento sia effettuato con mezzi automatici, senza impedimenti e in un formato strutturato, di uso comune e leggibile i dati personali che La riguardano per trasmetterli ad altro titolare o &ndash; se tecnicamente fattibile &ndash; di ottenere la trasmissione diretta da parte del Titolare ad altro titolare;</li>
<li>opporsi, in tutto o in parte: a) per motivi legittimi connessi alla Sua situazione particolare, al trattamento dei dati personali che La riguardano, ancorch&eacute; pertinenti allo scopo della raccolta; b) al trattamento di dati personali che La riguardano a fini di invio di materiale pubblicitario o di vendita diretta o per il compimento di ricerche di mercato o di comunicazione commerciale, mediante l&rsquo;uso di sistemi automatizzati di chiamata senza l&rsquo;intervento di un operatore mediante email e/o mediante modalit&agrave; di marketing tradizionali mediante telefono e/o posta cartacea;</li>
<li>proporre reclamo all&rsquo;Autorit&agrave; Garante per la Protezione dei Dati Personali.</li>
</ul>
<p align="left">&nbsp;</p>
<p align="left">Nei casi di cui sopra, ove necessario, il Titolare porter&agrave; a conoscenza i soggetti terzi ai quali i Suoi dati personali sono comunicati dell&rsquo;eventuale esercizio dei diritti da parte Sua, ad eccezione di specifici casi (es. quando tale adempimento si riveli impossibile o comporti un impiego di mezzi manifestamente sproporzionato rispetto al diritto tutelato).</p>
<p align="left">&nbsp;</p>
<ol start="10">
<li>Modalit&agrave; di esercizio dei diritti</li>
</ol>
<p align="left">&nbsp;</p>
<p align="left">Potr&agrave; in qualsiasi momento esercitare tali diritti:</p>
<ul>
<li>inviando una raccomandata a.r. all&rsquo;indirizzo del Titolare; Servizi Italia S.p.A., in Castellina di Soragna (PR), Via San Pietro 59/B &ndash; 43019</li>
<li>inviando una mail a privacy@si-servizitalia.com</li>
<li>telefonando al numero (+39) 0524 598511</li>
</ul>
<p align="left">&nbsp;</p>
<ol start="11">
<li>Titolare e responsabile del trattamento</li>
</ol>
<p align="left">&nbsp;</p>
<p align="left">Il titolare del trattamento &egrave;:</p>
<ul>
<li>Servizi Italia S.p.A., con sede legale in Castellina di Soragna (PR), Via San Pietro 59/B &ndash; 43019, C.F. 08531760158 e P.IVA 02144660343.</li>
</ul>
<p align="left">&nbsp;</p>
<p align="left">Il Responsabile Privacy (Chief Privacy Officer) nominato &egrave;:</p>
<ul>
<li>Dott.ssa Daniela Felloni;</li>
</ul>
<ul>
<li>Email: <a href="mailto:cpo@si-servizitalia.com">cpo@si-servizitalia.com</a>;</li>
</ul>
<ul>
<li>Telefono: (+39) 0524 598511.</li>
</ul>
<p align="left">&nbsp;</p>
<p align="left">L&rsquo;elenco aggiornato dei responsabili del trattamento &egrave; custodito presso la sede del Titolare in Via San Pietro 59/B &ndash; 43019 Castellina di Soragna (PR).</p>
<p align="left">&nbsp;</p>
<p align="left">&nbsp;</p>
<p align="left">Castellina di Soragna, 25 maggio 2018</p>

            </div>
      <div class="clearfix"></div>
      </div>
    </div>
  </div>
</section>
','{projectname}','privacy');
INSERT INTO public.pagine (uuid,body,progetto,name) VALUES ('gestione-richieste','
<section class="main">

<div class="main-content">
    <div class="breadcrumbs">
      <div class="container">
        <div class="col-sm-4">
          <h1>Gestione Richieste</h1>
        </div>
      </div>
    </div>
    
    <div class="container relative">
        <div class="col-sm-12">   
      <div class="list-wrapper">
        <div class="row">

      <div class="frame-box clearfix">
                <iframe src="https://{projectname}.si-portal.it/messages-ng" width="100%" style="border: 0px; height: 105vh" height="600"></iframe>
            </div>
      <div class="clearfix"></div>
      </div>
    </div>
  </div>
</section>
','{projectname}','gestione richieste');
INSERT INTO public.pagine (uuid,body,progetto,name) VALUES ('repository-documentale','
<section class="main">

<div class="main-content">
    <div class="breadcrumbs">
      <div class="container">
        <div class="col-sm-4">
          <h1>Repository Documentale</h1>
        </div>
      </div>
    </div>
    
    <div class="container relative">
        <div class="col-sm-12">   
      <div class="frame-box clearfix">
                <iframe src="https://{projectname}.si-portal.it/documents-ng" width="100%" style="border: 0px; height: 105vh" height="600"></iframe>
            </div>
      </div>
      <div class="clearfix"></div>
  </div>
</section>
','{projectname}','repository documentale');
INSERT INTO public.progetti (uuid,footer,logo_piccolo,logo,name,header) VALUES ('{projectname}','<div class="footer" > <div class="container"> <div class="col-sm-12"> <div class="menu-footer"> <ul> <li><a href="/#/privacy">Privacy</a></li> <li><a href="/#/cookies" >Cookies</a></li> <li><a href="/#/mappa-del-sito">Mappa del sito</a></li> </ul> </div> </div> </div> </div>',NULL,NULL,'{projectname}','<div class="col-sm-3"> <a class="logo pull-left" href="/#/homepage"><img src="/assets/images/logo.png"></a> </div><div class="col-sm-5 text-center hidden-nav"> <h1>{projectname_allcaps}</h1> </div>');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('admin','all rules','metadatas',NULL,NULL);
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('conditions_view','conditions view rule','metadatas','conditions','view');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('conditions_edit','conditions edit rule','metadatas','conditions','edit');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('documents_view','documents view rule','metadatas','documents','view');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('documents_edit','documents edit rule','metadatas','documents','edit');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('extensions_view','extensions view rule','extensions','extensions','view');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('extensions_edit','extensions edit rule','extensions','extensions','edit');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('fielddefinitions_view','fielddefinitions view rule','metadatas','fielddefinitions','view');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('fielddefinitions_edit','fielddefinitions edit rule','metadatas','fielddefinitions','edit');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('links_view','links view rule','metadatas','links','view');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('links_edit','links edit rule','metadatas','links','edit');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('metadatas_view','metadatas metadatas rule','metadatas','metadatas','view');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('metadatas_edit','metadatas metadatas rule','metadatas','metadatas','edit');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('roles_view','roles view rule','metadatas','roles','view');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('roles_edit','roles edit rule','metadatas','roles','edit');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('selectqueries_view','selectqueries view rule','metadatas','selectqueries','view');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('selectqueries_edit','selectqueries edit rule','metadatas','selectqueries','edit');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('urlmaprules_view','urlmaprules view rule','metadatas','urlmaprules','view');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('urlmaprules_edit','urlmaprules edit rule','metadatas','urlmaprules','edit');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('users_view','users view rule','metadatas','users','view');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('users_edit','users edit rule','metadatas','users','edit');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('draggables_edit','draggables edit rule','metadatas','draggables','edit');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('draggables_view','draggables view rule','metadatas','draggables','view');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('droppables_edit','droppables edit rule','metadatas','droppables','edit');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('droppables_view','droppables view rule','metadatas','droppables','view');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('contents_view','contents view rule','metadatas',NULL,'view');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('contents_edit','contents edit rule','metadatas',NULL,'edit');
INSERT INTO public.roles (name,description,object_type,object_name,action) VALUES ('publicdata_edit','publicdata edit rule','metadatas','publicdata','edit');
INSERT INTO public.userroles (username,role) VALUES ('admin','admin');
INSERT INTO public.users (username,password,name,surname,email,active,creation_date,last_update_date) VALUES ('admin','jGl25bVBBBW96Qi9Te4V37Fnqchz/Eu4qB9vKrRIqRg=','admin','admin','admin@snello.io',true,NULL,NULL);
